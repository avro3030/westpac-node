// Node process is an event emitter
process.on('exit', (code) => {
    // when process exits we can do any final synchronous operation
    // before node process terminates
});

process.on('uncaughtException', (err) => {
    // if anything goes unhandled
    // In that case we can do some cleanup here and exit anyway

    // although we should not do that, as process will keep running
    console.log(err);

    // if we have to use it anyway, we must force the process to exit
    process.exit(1);
});

// to keep the node event loop busy
process.stdin.resume();

// To Trigger a TypeError Exception
console.dog();

