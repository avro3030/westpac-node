const {StringDecoder} = require('string_decoder');
const stringDecoder = new StringDecoder('utf8');

process.stdin.on('readable', () => {
    const dataChunk = process.stdin.read();
    if (dataChunk != null) {
        const buffer = Buffer.from([dataChunk]);
        console.log('Result with .toString() : ', buffer.toString());
        console.log('Result with StringDecoder : ', stringDecoder.write(buffer));
    }
});