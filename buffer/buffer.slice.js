const fs = require('fs');
const conversionMap = {'88': '65', '89': '66', '90': '67'};

fs.readFile(__filename, (err, bufferData) => {
    let data = bufferData.slice(-3);
    data.forEach((item, i) => data[i] = conversionMap[item]);
    console.log(bufferData.toString());
});

// this will become abc
// TAG : XYZ