const someString = 'Here goes some string data';
const someBuffer = Buffer.from('Here goes some string data');

console.log(someString, someString.length);
console.log(someBuffer, someBuffer.length);

// Buffer is very useful when we need to read things from image file
// from a tcp stream, compressed file or any other form of binary access data

// when converting streams of binary data we should use StringDecoder mode
// because it handles multi-byte character much better
// specially incomplete one