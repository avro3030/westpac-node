const EventEmitter = require('events');

class Server extends EventEmitter {
    constructor(client) {
        super();
        this.tasks = {};
        this.taskId = 1;
        client.on('command', (command, args) => {

            switch (command) {
                case 'add':
                    this.add(args);
                    break;
                case 'list':
                    this.list(args);
                    break;
                case 'delete':
                    this.delete(args);
                    break;
                case 'help':
                    this.help();
                    break;
                default:
                    this.emit('response', `Unknown command ${command} ${args}`);
            }
            // console.log(`Client sent me : ${input} `);
        });
    }

    add(args) {
        this.tasks[this.taskId] = args.join(' ');
        this.emit('response', `Added task ${this.taskId}:  ${this.tasks[this.taskId]}`);
        this.taskId++;
    }

    help() {
        this.emit('response', `
        Type: add 'your work' - to add a work,
        Type: list - to list currently added works,
        Type: delete id - to delete a work,
        `)
    }

    list(args) {
        let alltasks = [];
        let i = 1;
        for (let task in this.tasks) {
            alltasks.push(`${task}: ${this.tasks[task]}`);
            i++;
        }
        this.emit('response', alltasks.join('\n'));
    }

    delete(args) {
        let deleted = this.tasks[args[0]];
        delete this.tasks[args[0]];
        this.emit('response', `You have deleted task ${args[0]}: ${deleted}`);
    }
}

module.exports = (client) => new Server(client);