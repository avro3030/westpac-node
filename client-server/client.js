const readline = require('readline');
const EventEmitter = require('events');


const r1 = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

const client = new EventEmitter();

let command, args;

r1.on('line', (input) => {
    [command, ...args] = input.split(' ');
    client.emit('command', command, args);
});

const server = require('./server')(client);

server.on('response', (resp) => {
    console.log(resp);
});





