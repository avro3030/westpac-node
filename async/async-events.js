const EventEmitter = require('events');
const fs = require('fs');

class AsyncWithTime extends EventEmitter {
    execute(asyncFunction, ...args) {
        console.time('execute');
        this.emit('begin');
        asyncFunction(...args, (err, data) => {
            if (err) return this.emit('error', err);
            this.emit('data', data);
            console.timeEnd('execute');
            this.emit('end');
        });
    }
}

const asyncWithTime = new AsyncWithTime();

asyncWithTime.on('begin', () => console.log('execution start'));
asyncWithTime.on('end', () => console.log('execution is done!'));

asyncWithTime.execute(fs.readFile, __filename);
