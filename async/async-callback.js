const fs = require('fs');
const http = require('http');

const readingFileAsArray = (file, cb) => {
    fs.readFile(file, (err, data) => {
        if (err) return cb(err);
        cb(null, data.toString().trim().split('\n'));
    });
};


readingFileAsArray('./numbers', (err, lines) => {
    if (err) throw err;
    const numbers = lines.map(Number);
    const oddNumbers = numbers.filter(number => number % 2 === 1);
    console.log('odd numbers count:', oddNumbers.length);
});
